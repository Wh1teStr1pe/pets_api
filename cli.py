import asyncio
import datetime as dt
import json

import click
from sqlalchemy import func
from sqlalchemy.future import select

from pets_api.database import async_session
from pets_api.models.pets import Pet
from pets_api.utils.tools import make_media_url


async def get_pets(has_photos: bool) -> dict:
    async with async_session() as session:
        if has_photos:
            stmt = select(Pet) \
                .join(Pet.images)\
                .group_by(Pet.id).having(func.count(Pet.images) > 0)
        else:
            stmt = select(Pet)
        stmt = stmt.order_by(-Pet.id)
        result = await session.execute(stmt)
        pets = result.unique().scalars().all()
        url = make_media_url()
        return {"pets": [await p.dict(url, from_dt_to_str=True) for p in pets]}


@click.command()
@click.option(
    '--has_photos', '-hp',
    default=False,
    type=bool,
    help='Parameter to get pets data with photos from database',
)
def main(has_photos: bool):
    """
    A tool that downloads pets data and saves it to json file.
    Provide optional parameter "-hp True"
    (default value is False) in order to get information about pets that have photos in database.
    Here are two examples:
    1. python3 cli.py -hp true
    2. python3 cli.py --has_photos false
    """
    loop = asyncio.get_event_loop()
    pets_data = loop.run_until_complete(get_pets(has_photos))
    file_name = f"{dt.datetime.now().strftime('%d.%m.%Y-%H:%M')}.json"
    with open(file_name, "w") as file:
        json.dump(pets_data, file, ensure_ascii=False, indent=4)


if __name__ == "__main__":
    main()
