# FastAPI Microservice for pets

## pets_api
Simple api with REST architecture

## Description

This is a fully async [FastAPI](https://fastapi.tiangolo.com/) project with async [SQLAlchemy ORM](https://docs.sqlalchemy.org/en/14/index.html).

The full stack of this project is composed by:

* [FastAPI](https://fastapi.tiangolo.com/) - A modern, fast (high-performance), web framework for building APIs with Python 3.6+ based on standard Python type hints.
* [SQLAlchemy ORM](https://docs.sqlalchemy.org/en/14/index.html) - SQLAlchemy is the Python SQL toolkit and Object Relational Mapper that gives application developers the full power and flexibility of SQL.
* [SQLite](https://sqlite.org/index.html) - SQLite is a C-language library that implements a small, fast, self-contained, high-reliability, full-featured, SQL database engine.

## How to run project

### To run by using docker

 - Make sure you have installed `docker` and `docker-compose`
 - Run `
docker-compose -f docker-compose.dev.yml up --build`

### To run by starting uvicorn

 - Create virtual environment `virtualenv venv`
 - Activate it `source venv/bin/activate`
 - Install requirements `pip install -r requirements.txt`
 - Run `python3 main.py`

## Settings
There are several types of config:
 - Dev
 - Stage
 - Prod

All variables store in `.env` file.
After running service, variables load to environment.
For each type of config there is a prefix for variables. After running service, variables parses by using BaseSettings class from pydantic.



In order to change `HOST`/`PORT`, or turn off hot-reload etc - you can achieve it by changing values in `.env` file.

### Example of `.env` ###
![img.png](env_example.png)

## Usage
You can find all the information you need for using API by going to `PROTOCOL://HOST:PORT/docs`; http://0.0.0.0/8080/docs by default.

### Example of swagger docs ###
![img.png](docs_example.png)
