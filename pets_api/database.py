from datetime import datetime
from typing import List, Optional

from sqlalchemy import delete, MetaData, Column, Integer, TIMESTAMP, text
from sqlalchemy.exc import NoResultFound
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.future import select
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.functions import current_timestamp

from pets_api.config import config

engine = create_async_engine(config.DB_URL.get_secret_value(), echo=config.ECHO, future=True)
async_session = sessionmaker(
    engine,
    class_=AsyncSession,
    expire_on_commit=False,
    future=True
)


async def get_session() -> AsyncSession:
    async with async_session() as session:
        yield session


metadata = MetaData()
Base = declarative_base(metadata=metadata)


class BaseModel(Base):
    __abstract__ = True
    __mapper_args__ = {"eager_defaults": True}

    id = Column(Integer, primary_key=True, autoincrement=True)
    created_at = Column(TIMESTAMP(timezone=False), server_default=current_timestamp())
    updated_at = Column(TIMESTAMP(timezone=False))

    async def save(
            self,
            session: AsyncSession,
            commit: Optional[bool] = True
    ):
        session.add(self)
        if commit:
            try:
                await session.commit()
            except Exception as e:
                await session.rollback()
                raise e

    @classmethod
    async def get_by_id(
            cls,
            session: AsyncSession,
            id: int,
    ):
        """Получение объекта по id"""
        return await session.get(cls, id)

    @classmethod
    async def get_objects(
            cls,
            session: AsyncSession,
            fields: Optional[list] = None,
            params: Optional[dict] = None,
            sorts: Optional[str] = None,
            offset: Optional[int] = None,
            limit: Optional[int] = None,
    ):
        """Получение объектов"""
        if not fields:
            fields = [cls]
        if params is None:
            params = {}
        if sorts is None:
            sorts = -cls.id
        else:
            sorts = text(sorts)
        stmt = select(*fields).filter_by(**params)
        stmt = stmt.order_by(sorts).offset(offset).limit(limit)
        result = await session.execute(stmt)
        return result.scalars().all()

    @classmethod
    async def create_object(
            cls,
            session: AsyncSession,
            data: dict,
    ):
        """Создание объекта"""
        entity = cls(**data)
        session.add(entity)
        await session.flush()
        await session.refresh(entity)

        try:
            await session.commit()
            return entity
        except Exception:
            await session.rollback()

    async def update_object(
            self,
            session: AsyncSession,
            data: dict
    ):
        """Обновление объекта"""
        for k, v in data.items():
            setattr(self, k, v)
        self.updated_at = datetime.now()
        await session.commit()

    async def delete_object(
            self,
            session: AsyncSession,
    ):
        """Удаление объекта"""
        try:
            await session.delete(self)
            await session.commit()
            return True
        except Exception:
            await session.rollback()
            return False

    @classmethod
    async def delete_by_id(
            cls,
            session: AsyncSession,
            id: int
    ):
        """Удаление объекта по ID"""
        try:
            obj = await session.get(cls, id)
            if not obj:
                raise NoResultFound
            await session.delete(obj)
            await session.commit()
            return True
        except NoResultFound:
            await session.rollback()
            return False

    @classmethod
    async def bulk_delete(
            cls,
            session: AsyncSession,
            ids: List[int]
    ) -> dict:
        """Удаление нескольких объектов по ID"""
        try:
            objects_ids = await session.scalars(select(cls.id).where(cls.id.in_(ids)))
            objects_ids = objects_ids.all()
            await session.execute(delete(cls).where(cls.id.in_(objects_ids)))
            await session.commit()
            not_deleted = list(filter(lambda x: x not in objects_ids, ids))
            result = dict(deleted=len(objects_ids))
            errors = [
                {"id": i, "error": f"{cls.__name__.capitalize()} with the matching ID was not found."}
                for i in not_deleted
            ]
            result.update(errors=errors)
            return result
        except Exception:
            await session.rollback()
            return {"deleted": 0, "errors": ["Something went wrong while deleting"]}
