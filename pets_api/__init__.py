import os

from dotenv import load_dotenv

from . import api, logic, schemas, validators

# Loading variables from .env file to environment
if os.getenv('ENV_STATE', 'dev') == 'dev':
    os.environ['ENV_STATE'] = 'dev'
    load_dotenv(dotenv_path='.env')
else:
    load_dotenv(dotenv_path='.env')


__all__ = [
    "api",
    "logic",
    "schemas",
    "validators",
]