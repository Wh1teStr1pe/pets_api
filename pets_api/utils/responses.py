from http import HTTPStatus

from pets_api import schemas

responses = {
    "GET": {
        "pet": {
            HTTPStatus.NOT_FOUND.value: {"model": schemas.pets.Pet, "description": "The pet was not found"},
            HTTPStatus.OK.value: {"model": schemas.pets.Pet, "description": "Pet requested by ID"},
        },
        "pets": {
            HTTPStatus.OK.value: {"model": schemas.pets.Pets, "description": "Pets requested"},
            HTTPStatus.NOT_ACCEPTABLE.value: {"description": "Only .jpeg/.jpg or .png  files allowed"},
        },
        "files": {
            HTTPStatus.NOT_FOUND.value: {"description": "File not found"},
            HTTPStatus.NOT_ACCEPTABLE.value: {"description": "Only .jpeg/.jpg or .png  files allowed"},
        }
    },
    "POST": {
        "image": {
            HTTPStatus.OK.value: {"model": schemas.pets.Image, "description": "Image uploaded"},
            HTTPStatus.NOT_FOUND.value: {"model": schemas.pets.Image, "description": "Pet not found"},
            HTTPStatus.NOT_ACCEPTABLE.value: {"description": "Only .jpeg/.jpg or .png  files allowed"},
        }
    },
}
