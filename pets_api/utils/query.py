import json
from json import JSONDecodeError
from typing import List

from fastapi import Query
from sqlalchemy import Column
from sqlalchemy.orm import class_mapper

from pets_api.database import BaseModel
from pets_api import validators
from pets_api.utils.tools import get_subclass_by_name


class BaseQueryParams:
    def __init__(
            self,
            page: int = 1,
            quantity: int = 10
    ):
        self.limit, self.offset = self.validate_pagination(page, quantity)

    @staticmethod
    def validate_pagination(page, quantity):
        pagination = validators.PaginationValidator(page=page, quantity=quantity)
        offset = pagination.quantity * (pagination.page - 1)
        return pagination.quantity, offset

    @staticmethod
    def computed_operator(
            column: Column,
            operator: str,
            objects_type: str,
            value=None
    ):
        operators = {
            'gt': column.__gt__,
            'lt': column.__lt__,
            'ge': column.__ge__,
            'le': column.__le__,
            'eq': column.__eq__,
            'asc': column.asc,
            'desc': column.desc,
        }
        if objects_type == "filters":
            return operators.get(operator, column.__eq__)(value)

        elif objects_type == "sorts":
            return operators.get(operator, column.asc)()

    def create_binary_expressions(self, objects: List[dict], objects_type: str):
        expressions = list()
        for obj in objects:
            field, value = obj.get("field", ""), obj.get("value")
            model_name, operator = obj.get("model"), obj.get("operator")

            _class = get_subclass_by_name(BaseModel, model_name)
            if not _class:
                continue

            mapper = class_mapper(_class)
            if not hasattr(mapper.columns, field):
                continue

            column_type = mapper.columns[field].type.python_type
            if objects_type == "filters":

                if not value:
                    continue

                if column_type is int:
                    try:
                        valid_digit = validators.DigitFromStringValidator(value=value)
                        value = valid_digit.value
                    except TypeError:
                        continue

            expression = self.computed_operator(
                column=mapper.columns[field],
                operator=operator,
                objects_type=objects_type,
                value=value
            )
            expressions.append(expression)

        return list(filter(lambda item: item is not None, expressions))

    def validate_query(self, query: str, objects_type: str):

        if not query:
            return None

        try:
            objects = json.loads(query).get(objects_type, list())
        except JSONDecodeError:
            return None

        expressions = self.create_binary_expressions(objects, objects_type)
        return expressions


class CommonQueryParams(BaseQueryParams):
    def __init__(
            self,
            page: int = 1,
            quantity: int = 20,
            filters: str = Query(
                None,
                title="Filtering query",
                description="Query string to filter objects from the database",
                example="""{"filters": [{"model": "pet", "field": "age", "value": "0", "operator": "ge"}]}"""
            ),
            sorts: str = Query(
                None,
                title="Sorting query",
                description="Query string to sort objects from the database",
                example="""{"sorts": [{"model": "pet", "field": "id", "operator": "desc"}]}"""
            ),
    ):
        super().__init__(page, quantity)
        self.filters = self.validate_query(filters, "filters")
        self.sorts = self.validate_query(sorts, "sorts")
