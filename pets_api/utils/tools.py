import asyncio
import os
import uuid
from http import HTTPStatus
from typing import List

import aiofiles
from aiofiles.os import remove
from fastapi import HTTPException, UploadFile, APIRouter
from starlette.requests import Request

from pets_api.config import config


def get_subclass_by_name(_class, name: str):
    if not name:
        return None
    return next((c for c in _class.__subclasses__() if c.__name__.lower() == name.lower()), None)


def get_media_url(request: Request, router: APIRouter):
    return str(request.base_url) + router.prefix.strip("/") + config.MEDIA_DIR


def make_media_url():
    url = config.PROTOCOL + "://" + config.SERVER_HOST + ":" + \
          str(config.SERVER_PORT) + config.PETS_PREFIX + config.MEDIA_DIR
    return url


async def save_image(image: UploadFile) -> str:
    if not await aiofiles.os.path.exists(config.MEDIA_ROOT):
        await aiofiles.os.mkdir(config.MEDIA_ROOT)

    if image.content_type not in ["image/jpeg", "image/jpg", "image/png"]:
        raise HTTPException(
            status_code=HTTPStatus.NOT_ACCEPTABLE,
            detail="Only .jpeg/.jpg or .png  files allowed"
        )

    file_type = image.content_type.split("/", maxsplit=1)[-1]
    file_name = ".".join([str(uuid.uuid4().hex), file_type])
    file_path = os.path.join(config.MEDIA_ROOT, file_name)

    async with aiofiles.open(file_path, "wb") as file:
        content = await image.read()
        await file.write(content)

    return file_name


async def delete_images(images: List[str]):
    tasks = list()
    images = [os.path.join(config.MEDIA_ROOT, i) for i in images]
    for image in images:
        exists = await aiofiles.os.path.exists(image)
        if not exists:
            continue
        tasks.append(remove(image))
    if tasks:
        await asyncio.wait(tasks)
