import datetime
from enum import Enum
from typing import Optional, Any, List

from pydantic import BaseModel, validator


class IDMixin(BaseModel):
    id: int


class Gender(str, Enum):
    female = 'female'
    male = 'male'


class Type(str, Enum):
    dog = 'dog'
    cat = 'cat'


class Image(BaseModel):
    id: int
    name: str
    url: str = None


class PetBase(BaseModel):
    name: str
    age: int
    gender: Gender
    type: Type

    class Config:
        orm_mode = True

    @validator('age', allow_reuse=True)
    def page_must_be_positive(cls, v):
        if v is not None and v < 0 <= 30:
            raise ValueError('Age must be positive')
        return v


class PetIn(PetBase):
    pass


class PetUpdateIn(PetBase):
    name: str = None
    age: int = None
    gender: Gender = None
    type: Type = None


class Pet(PetBase, IDMixin):
    images: List[Image]
    created_at: datetime.datetime
    updated_at: Optional[datetime.datetime]


class Pets(BaseModel):
    count: int
    items: List[Pet]


class PetsDeleteIn(BaseModel):
    ids: List[int]
