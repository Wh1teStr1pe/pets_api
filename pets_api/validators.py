from pydantic import BaseModel, validator, ValidationError


class PaginationValidator(BaseModel):
    page: int
    quantity: int

    @validator('page')
    def page_must_be_positive(cls, v):
        if v <= 0:
            raise ValueError('Page must be greater than zero')
        return v

    @validator('quantity')
    def quantity_must_be_positive(cls, v):
        if v <= 0:
            raise ValueError('Quantity must be greater than zero')
        return v


class DigitFromStringValidator(BaseModel):
    value: str

    @validator('value')
    def value_must_be_an_integer(cls, v):
        if not v.isdigit():
            raise TypeError('Value must be an numeric')
        return int(v)
