import os
from http import HTTPStatus
from typing import Optional

from fastapi import Header, HTTPException
from pydantic import BaseSettings, Field, SecretStr


async def get_token_header(x_api_key: str = Header(...)):
    if x_api_key != os.getenv("API_KEY", ""):
        raise HTTPException(status_code=HTTPStatus.BAD_REQUEST, detail="X-API-KEY header invalid")


class BaseConfig(BaseSettings):
    """Base config."""

    API_KEY: SecretStr = None
    DB_URL: SecretStr = None
    ECHO: bool = False
    ENV_STATE: Optional[str] = Field("dev", env="ENV_STATE")
    MEDIA_DIR: str = "/media/"
    MEDIA_ROOT: str = os.path.join(os.getcwd(), "media")
    PETS_PREFIX: str = "/pets"
    PROTOCOL: str = "http"
    SERVER_HOST: str = "0.0.0.0"
    SERVER_PORT: int = 8000
    RELOAD: bool = False

    class Config:
        """Loads the dotenv file"""

        env_file: str = ".env"
        fields = {
            "API_KEY": {
                "env": "API_KEY"
            },
        }


class DevConfig(BaseConfig):
    """Development config"""

    class Config:
        env_prefix: str = "DEV_"


class ProdConfig(BaseConfig):
    """Production config"""

    class Config:
        env_prefix: str = "PROD_"


class StageConfig(BaseConfig):
    """Stage config"""

    class Config:
        env_prefix: str = "STAGE_"


class FactoryConfig:
    """Returns a config instance depending on the ENV_STATE variable"""

    def __init__(self, env_state: Optional[str]):
        self.env_state = env_state

    def __call__(self):
        if self.env_state == "dev":
            return DevConfig()

        elif self.env_state == "prod":
            return ProdConfig()

        elif self.env_state == "stage":
            return StageConfig()


config = FactoryConfig(env_state=BaseConfig().ENV_STATE)()
