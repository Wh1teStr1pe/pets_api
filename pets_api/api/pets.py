import os
from http import HTTPStatus

from fastapi import (
    APIRouter,
    Depends, Path, UploadFile, File, HTTPException, Query, )
from fastapi.responses import FileResponse
from sqlalchemy.ext.asyncio import AsyncSession
from starlette.requests import Request

from pets_api import schemas, logic
from pets_api.config import config
from pets_api.database import get_session
from pets_api.utils.query import CommonQueryParams
from pets_api.utils.responses import responses
from pets_api.utils.tools import get_media_url

router = APIRouter(
    prefix=config.PETS_PREFIX,
    tags=['pets'],
)


@router.get('/', responses=responses.get("GET").get("pets"))
async def read_pets(
        request: Request,
        session: AsyncSession = Depends(get_session),
        has_photos: bool = Query(False, title="Parameter to get pets with photos"),
        commons: CommonQueryParams = Depends()
) -> dict:
    media_url = get_media_url(request, router)
    pets = await logic.pets.get_pets(
        session=session,
        has_photos=has_photos,
        commons=commons,
        media_url=media_url
    )
    return pets


@router.get(path='/{pet_id}', responses=responses.get("GET").get("pet"))
async def get_pet(
        request: Request,
        pet_id: int = Path(..., titile="The ID of the pet to get"),
        session: AsyncSession = Depends(get_session)
) -> dict:
    media_url = get_media_url(request, router)
    pet = await logic.pets.get_pet(
        pet_id=pet_id,
        session=session,
        media_url=media_url
    )
    return pet


@router.post('/', responses=responses.get("GET").get("pet"))
async def create_pet(
        request: Request,
        data: schemas.pets.PetIn = Depends(),
        image: UploadFile = File(None, description="Jpeg/Png file as UploadFile"),
        session: AsyncSession = Depends(get_session),
) -> dict:
    media_url = get_media_url(request, router)
    return await logic.pets.create_pet(
        session=session,
        data=data,
        image=image,
        media_url=media_url
    )


@router.put('/{pet_id}', responses=responses.get("GET").get("pet"))
async def update_pet(
        request: Request,
        pet_id: int = Path(..., title="The ID of the pet to get"),
        data: schemas.pets.PetUpdateIn = Depends(),
        session: AsyncSession = Depends(get_session)
):
    media_url = get_media_url(request, router)
    pet = await logic.pets.update_pet(
        session=session,
        pet_id=pet_id,
        data=data,
        media_url=media_url
    )
    return pet


@router.delete('/{pet_id}')
async def delete_pet(
        pet_id: int = Path(..., title="The ID of the pet to get"),
        session: AsyncSession = Depends(get_session),
) -> dict:
    status = await logic.pets.delete_pet(
        session=session,
        pet_id=pet_id,
    )
    return status


@router.delete('/')
async def delete_many_pets(
        data: schemas.pets.PetsDeleteIn,
        session: AsyncSession = Depends(get_session),
) -> dict:
    deleted = await logic.pets.delete_many_pets(
        session=session,
        ids=data.ids,
    )
    return deleted


@router.post('/{pet_id}/photo', responses=responses.get("POST").get("image"))
async def upload_photo(
        pet_id: int = Path(..., title="The ID of the pet to for uploading new photo"),
        image: UploadFile = File(..., description="Jpeg/Png file as UploadFile"),
        session: AsyncSession = Depends(get_session),
):
    image = await logic.pets.upload_pet_image(session=session, pet_id=pet_id, image=image)
    return image


@router.get('/media/{file_name}', responses=responses.get("GET").get("files"))
async def get_pet_picture(
        file_name: str = Path(
            ...,
            description="File name in order to download pet's picture"
        )
):
    if any(
            [
                (file_name.endswith(".jpg")),
                (file_name.endswith(".jpeg")),
                (file_name.endswith(".png"))
            ]
    ):
        file_path = os.path.join(config.MEDIA_ROOT, file_name)
        if not os.path.exists(file_path):
            raise HTTPException(status_code=HTTPStatus.NOT_FOUND, detail="File not found")

        response = FileResponse(path=file_path, filename=file_name)
        return response
    else:
        raise HTTPException(status_code=HTTPStatus.NOT_ACCEPTABLE, detail="Only .jpeg/.jpg or .png  files allowed")
