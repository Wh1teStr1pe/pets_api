from fastapi import APIRouter

from pets_api.api import pets

router = APIRouter()

router.include_router(pets.router)
