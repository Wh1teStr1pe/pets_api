from fastapi import FastAPI, Depends

from pets_api import api
from pets_api.config import get_token_header

app = FastAPI(
    dependencies=[Depends(get_token_header)]
)

app.include_router(api.router)
