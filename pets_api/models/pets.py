from typing import Optional, List

from sqlalchemy import String, Column, Integer, ForeignKey, func
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
from sqlalchemy.orm import relationship

from pets_api.database import BaseModel


class Pet(BaseModel):
    __tablename__ = 'pet'

    name = Column(String(length=100), nullable=False)
    age = Column(Integer, nullable=False)
    gender = Column(String(length=50), nullable=False)
    type = Column(String(length=25), nullable=False)
    images = relationship(
        'Image',
        back_populates='pet',
        lazy='joined',
        passive_deletes=True,
        uselist=True
    )

    async def dict(self, url: str = None, from_dt_to_str=False):
        created_at = self.created_at
        updated_at = self.updated_at if self.updated_at else None

        if from_dt_to_str is True:
            created_at = str(self.created_at)
            updated_at = str(self.updated_at) if self.updated_at else None

        return {
            "id": self.id,
            "name": self.name,
            "age": self.age,
            "gender": self.gender,
            "type": self.type,
            "images": [await i.dict(url) for i in self.images],
            "created_at": created_at,
            "updated_at": updated_at
        }

    @classmethod
    async def create_pet(
            cls,
            session: AsyncSession,
            data: dict,
            image_name: str = None
    ):
        pet = await cls.create_object(session=session, data=data)
        if image_name:
            image = await Image.create_object(session, {"pet_id": pet.id, "name": image_name})
            pet.images.append(image)
        await session.commit()
        return pet

    @classmethod
    async def get_pets(
            cls,
            session: AsyncSession,
            has_photos: bool = False,
            filters: Optional[list] = None,
            sorts: Optional[list] = None,
            offset: Optional[int] = 1,
            limit: Optional[int] = 20,
    ):
        """Получение животных"""
        if filters is None:
            filters = []
        if sorts is None:
            sorts = [-cls.id]
        if has_photos is True:
            stmt = select(cls) \
                .join(cls.images).filter(*filters) \
                .group_by(cls.id).having(func.count(cls.images) > 0)
        else:
            stmt = select(cls).filter(*filters)
        stmt = stmt.order_by(*sorts).offset(offset).limit(limit)
        result = await session.execute(stmt)
        return result.unique().scalars().all()


class Image(BaseModel):
    __tablename__ = 'image'

    pet_id = Column(Integer, ForeignKey('pet.id', ondelete='CASCADE'), nullable=False)
    name = Column(String(length=250), nullable=False)
    pet = relationship(
        'Pet',
        back_populates='images',
        passive_deletes=True,
        uselist=False,
        lazy='joined',
    )

    async def dict(self, url: str = None):
        data = {"id": self.id, "name": self.name}
        if url:
            data.update(url=url + self.name)
        return data

    @classmethod
    async def get_pets_images(cls, session: AsyncSession, ids: List[int]):
        images = await session.execute(
            select(cls).where(cls.pet_id.in_(ids))
        )
        images = images.unique().scalars().all()
        return images
