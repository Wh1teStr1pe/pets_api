from http import HTTPStatus
from typing import List

from fastapi import HTTPException, UploadFile
from sqlalchemy.ext.asyncio import AsyncSession

from pets_api import schemas, models
from pets_api.utils.query import CommonQueryParams
from pets_api.utils.tools import save_image, delete_images


async def get_pets(
        session: AsyncSession,
        has_photos: bool,
        commons: CommonQueryParams,
        media_url: str,
) -> dict:
    pets = await models.pets.Pet.get_pets(
        session=session,
        has_photos=has_photos,
        filters=commons.filters,
        sorts=commons.sorts,
        offset=commons.offset,
        limit=commons.limit
    )
    pets = [await p.dict(media_url) for p in pets]
    return {"count": len(pets), "items": pets}


async def get_pet(
        pet_id: int,
        media_url: str,
        session: AsyncSession,
) -> dict:
    pet = await models.pets.Pet.get_by_id(session, pet_id)
    if not pet:
        raise HTTPException(
            status_code=HTTPStatus.NOT_FOUND,
            detail=HTTPStatus.NOT_FOUND.phrase
        )
    return await pet.dict(media_url)


async def create_pet(
        data: schemas.pets.PetIn,
        image: UploadFile,
        media_url: str,
        session: AsyncSession,
) -> dict:
    if not image:
        pet = await models.pets.Pet.create_pet(session=session, data=data.dict())
        return await pet.dict(media_url)

    file_name = await save_image(image)
    data = data.dict()
    pet = await models.pets.Pet.create_pet(session=session, data=data, image_name=file_name)
    return await pet.dict(media_url)


async def update_pet(
        pet_id: int,
        data: schemas.pets.PetUpdateIn,
        media_url: str,
        session: AsyncSession,
) -> dict:
    pet = await models.pets.Pet.get_by_id(session, pet_id)
    if not pet:
        raise HTTPException(
            status_code=HTTPStatus.NOT_FOUND,
            detail=HTTPStatus.NOT_FOUND.phrase
        )
    await pet.update_object(session, data.dict(exclude_none=True))
    return await pet.dict(url=media_url)


async def upload_pet_image(
        pet_id: int,
        image: UploadFile,
        session: AsyncSession
) -> dict:
    pet = await models.pets.Pet.get_by_id(session, pet_id)
    if not pet:
        raise HTTPException(
            status_code=HTTPStatus.NOT_FOUND,
            detail=HTTPStatus.NOT_FOUND.phrase
        )
    file_name = await save_image(image)
    image = await models.pets.Image.create_object(session, {"name": file_name, "pet_id": pet_id})
    return await image.dict()


async def delete_pet(
        pet_id: int,
        session: AsyncSession,
) -> dict:
    images = await models.pets.Image.get_pets_images(session, [pet_id])
    status = await models.pets.Pet.delete_by_id(session, pet_id)
    if images:
        await delete_images([i.name for i in images])
        await models.pets.Image.bulk_delete(session, [i.id for i in images])
    return {"deleted": status}


async def delete_many_pets(
        ids: List[int],
        session: AsyncSession,
) -> dict:
    images = await models.pets.Image.get_pets_images(session, ids)
    deleted = await models.pets.Pet.bulk_delete(session, ids)
    if images:
        await delete_images([i.name for i in images])
        await models.pets.Image.bulk_delete(session, [i.id for i in images])
    return deleted
