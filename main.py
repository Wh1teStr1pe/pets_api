import uvicorn

from pets_api.config import config

if __name__ == '__main__':
    uvicorn.run(
        'pets_api.app:app',
        host=config.SERVER_HOST,
        port=config.SERVER_PORT,
        reload=config.RELOAD
    )
